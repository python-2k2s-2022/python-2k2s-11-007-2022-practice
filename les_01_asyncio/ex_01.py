import asyncio


# Fix me!


def some_func():
    print('Before')
    asyncio.sleep(10)
    print('After')


async def main():
    some_func()


if __name__ == '__main__':
    asyncio.run(main())
