public class SortedArray extends ArrayWrapper {
    public SortedArray(IArray array){
        super(array);
    }

    @Override
    public String[] getArray() {
        String[] arr = array.getArray();
        Arrays.sort(arr);
        return arr;
    }
}
