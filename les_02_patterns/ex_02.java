public class EmptyTwoThreeTree implements ITwoThreeTree {
    private static EmptyTwoThreeTree twoThreeTree;
    private EmptyTwoThreeTree() {}

    public static EmptyTwoThreeTree getTwoThreeTree() {
        if (twoThreeTree == null) {
            if (twoThreeTree == null) {
                twoThreeTree = new EmptyTwoThreeTree();
            }
        }
        return twoThreeTree;
    }

    @Override
    public ITwoThreeTree add(int value) {
        return new TwoThreeTree(value);
    }
}
