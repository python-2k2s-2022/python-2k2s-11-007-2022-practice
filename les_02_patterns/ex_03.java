public class Main {
    public static void main(String[] args) {
        Color red = new RedColor();
        Color blue = new BlueColor();

        Shape redTri = new Triangle(red);
        Shape blueSqr = new Square(blue);

        redTri.applyColor();
        blueSqr.applyColor();
    }
}

... another file

public class Square extends Shape {
    public Square(Color c) {
        super(c);
    }

    @Override
    public void applyColor() {
        System.out.print("Square's color is ");
        color.applyColor();
    }
}
