public interface Criminal{
    public void addOfficer(Officer officer);
    public void removeOfficer(Officer officer);
    void notifyOfficers();
}
